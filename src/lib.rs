#![deny(missing_docs)]

//! The `multilinear-parser` library provides functionality to parse a multilinear system from a text-based format.
//! It allows you to define events, which rely on various channel specific conditions or changes using a markdown inspired syntax.
//!
//! Example Event Syntax:
//!
//! ```text
//! # Move to Livingroom
//!
//! place: bedroom > livingroom
//!
//! # Get Dressed
//!
//! place: bedroom
//! clothes: pajamas > casual
//! ```
//!
//! Supports logical combinations:
//!
//! ```text
//! (clothes: pajamas | clothes: casual) & place: bedroom
//! ```

use header_parsing::parse_header;
use logical_expressions::{LogicalExpression, ParseError};
use thiserror::Error;

use multilinear::{Channel, Condition, Event, EventInfo, InvalidChangeError, MultilinearInfo};

use std::{
    collections::HashMap,
    io::{BufRead, BufReader, Read},
};

#[derive(Copy, Clone, Debug)]
struct ValueCheckingError(char);

type Str = Box<str>;

fn check_name(name: &str) -> Result<(), ValueCheckingError> {
    if let Some(c) = name
        .chars()
        .find(|&c| !c.is_alphanumeric() && !"_- ".contains(c))
    {
        Err(ValueCheckingError(c))
    } else {
        Ok(())
    }
}

fn valid_name(name: &str) -> Result<&str, ValueCheckingError> {
    let name = name.trim();
    check_name(name)?;
    Ok(name)
}

struct ValueMap {
    map: HashMap<Str, usize>,
}

impl ValueMap {
    fn into_array(self) -> Box<[Str]> {
        let mut result = vec!["".into(); self.map.len()].into_boxed_slice();
        for (name, index) in self.map {
            result[index] = name;
        }
        result
    }
}

impl ValueMap {
    fn new() -> Self {
        let mut result = Self {
            map: HashMap::new(),
        };
        let _ = result.index("");
        result
    }

    fn index(&mut self, name: &str) -> Result<usize, ValueCheckingError> {
        let name = valid_name(name)?;

        if !self.map.contains_key(name) {
            let index = self.map.len();
            self.map.insert(name.into(), index);
        }

        let name = self.map.get_mut(name).unwrap();
        Ok(*name)
    }
}

/// Represents errors that can occur when parsing conditions.
#[derive(Copy, Clone, Debug, Error)]
pub enum ConditionParsingError {
    /// Indicates an invalid character was encountered.
    #[error("Invalid character '{0}' for condition names")]
    InvalidCharacter(char),

    /// Indicates an invalid condition format.
    #[error("Invalid condition format")]
    InvalidCondition,
}

impl From<ValueCheckingError> for ConditionParsingError {
    fn from(ValueCheckingError(c): ValueCheckingError) -> Self {
        Self::InvalidCharacter(c)
    }
}

/// Represents the kinds of errors that can occur when parsing a line.
#[derive(Copy, Clone, Debug, Error)]
pub enum LineErrorKind {
    /// Indicates an error occurred while parsing the line.
    #[error("Input error while parsing line")]
    LineParsing,

    /// Indicates an error occurred while parsing an expression.
    #[error("Parsing expression failed: {0}")]
    ExpressionParsing(ParseError<ConditionParsingError>),

    /// Indicates conflicting conditions were encountered.
    #[error("Encountered conflicting conditions: {0}")]
    ConflictingCondition(InvalidChangeError),

    /// Indicates an invalid character was encountered while parsing the event name.
    #[error("Invalid character '{0}' in event name")]
    InvalidCharacterInEventName(char),

    /// Indicates no event was specified.
    #[error("No event has been specified")]
    NoEvent,

    /// Indicates a subheader was encountered without a corresponding header.
    #[error("Subheader without matching header")]
    SubheaderWithoutHeader,
}

trait ErrorLine {
    type Output;

    fn line(self, line: usize) -> Self::Output;
}

impl ErrorLine for LineErrorKind {
    type Output = Error;

    fn line(self, line: usize) -> Error {
        Error::Line { line, kind: self }
    }
}

impl<T> ErrorLine for Result<T, LineErrorKind> {
    type Output = Result<T, Error>;

    fn line(self, line: usize) -> Result<T, Error> {
        match self {
            Ok(value) => Ok(value),
            Err(err) => Err(err.line(line)),
        }
    }
}

/// Represents errors that can occur during parsing.
#[derive(Debug, Error)]
pub enum Error {
    /// Indicates an error occurred on a specific line.
    #[error("Line {line}: {kind}")]
    Line {
        /// The line the error occured on.
        line: usize,
        /// The error kind.
        kind: LineErrorKind,
    },

    /// Indicates conflicting conditions were encountered.
    #[error("Conflicting conditions detected")]
    ConflictingCondition,
}

struct ChannelMap {
    map: HashMap<Str, (Channel, ValueMap)>,
}

impl ChannelMap {
    fn new() -> Self {
        Self {
            map: HashMap::new(),
        }
    }

    fn index(
        &mut self,
        name: &str,
        info: &mut MultilinearInfo,
    ) -> Result<(Channel, &mut ValueMap), ValueCheckingError> {
        let name = valid_name(name)?;

        if !self.map.contains_key(name) {
            let channel = info.add_channel();
            self.map.insert(name.into(), (channel, ValueMap::new()));
        }

        let (name, value) = self.map.get_mut(name).unwrap();
        Ok((*name, value))
    }
}

/// A multilinear info containing the mapped channel and event names.
pub struct NamedMultilinearInfo {
    /// The parsed `MultilinearInfo` instance.
    pub info: MultilinearInfo,
    /// A map associating events with their names.
    pub events: HashMap<Event, Vec<Str>>,
    /// A map associating channels with their names and the names of the channel.
    pub channels: HashMap<Channel, (Str, Box<[Str]>)>,
}

/// Parses a multilinear system from a reader.
///
/// This function reads the multilinear system definition from the provided reader and builds a `NamedMultilinearInfo` instance.
/// It returns the parsed `NamedMultilinearInfo` if successful, or an `Error` if parsing fails.
pub fn parse_multilinear<R: Read>(reader: R) -> Result<NamedMultilinearInfo, Error> {
    let mut info = MultilinearInfo::new();
    let mut current_event_name = Vec::new();
    let mut event_names = HashMap::new();
    let mut channel_names = ChannelMap::new();

    let mut condition_groups = Vec::new();
    let mut condition_lines = Vec::new();

    for (line_number, line) in BufReader::new(reader).lines().enumerate() {
        let Ok(line) = line else {
            return Err(LineErrorKind::LineParsing.line(line_number));
        };

        if line.trim().is_empty() {
            if !condition_lines.is_empty() {
                condition_groups.push(LogicalExpression::and(condition_lines));
                condition_lines = Vec::new();
            }
            continue;
        }

        if let Some(success) = parse_header(&mut current_event_name, &line) {
            let Ok(changes) = success else {
                return Err(LineErrorKind::SubheaderWithoutHeader.line(line_number));
            };

            if let Err(ValueCheckingError(c)) = check_name(&changes.header) {
                return Err(LineErrorKind::InvalidCharacterInEventName(c)).line(line_number);
            }

            if !condition_lines.is_empty() {
                condition_groups.push(LogicalExpression::and(condition_lines));
                condition_lines = Vec::new();
            }

            if !condition_groups.is_empty() {
                let mut event = EventInfo::new();
                for conditions in LogicalExpression::or(condition_groups).expand() {
                    if let Err(err) = event.add_change(&conditions) {
                        return Err(LineErrorKind::ConflictingCondition(err).line(line_number));
                    }
                }

                let event = info.add_event(event);
                event_names.insert(event, changes.path.clone());

                condition_groups = Vec::new();
            }

            changes.apply();

            continue;
        }

        if current_event_name.is_empty() {
            return Err(LineErrorKind::NoEvent.line(line_number));
        };

        let parse_conditions = |condition: &str| {
            let Some((channel, change)) = condition.split_once(':') else {
                return Err(ConditionParsingError::InvalidCondition);
            };

            let (channel, value_names) = channel_names.index(channel.trim(), &mut info)?;
            Ok(if let Some((from, to)) = change.split_once('>') {
                let from = value_names.index(from)?;
                let to = value_names.index(to)?;
                Condition::change(channel, from, to)
            } else {
                let change = value_names.index(change)?;
                Condition::new(channel, change)
            })
        };

        let conditions = LogicalExpression::parse_with(&line, parse_conditions);

        let conditions = match conditions {
            Ok(conditions) => conditions,
            Err(err) => return Err(LineErrorKind::ExpressionParsing(err).line(line_number)),
        };

        condition_lines.push(conditions);
    }

    if !condition_lines.is_empty() {
        condition_groups.push(LogicalExpression::and(condition_lines));
    }

    if !condition_groups.is_empty() {
        let mut event = EventInfo::new();
        for conditions in LogicalExpression::or(condition_groups).expand() {
            if event.add_change(&conditions).is_err() {
                return Err(Error::ConflictingCondition);
            }
        }

        let event = info.add_event(event);
        event_names.insert(event, current_event_name);
    }

    let channels = channel_names
        .map
        .into_iter()
        .map(|(name, (channel, values))| (channel, (name, values.into_array())))
        .collect();

    Ok(NamedMultilinearInfo {
        info,
        events: event_names,
        channels,
    })
}
